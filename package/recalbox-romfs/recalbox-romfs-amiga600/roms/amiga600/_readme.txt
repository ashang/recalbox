## RECALBOX - SYSTEM AMIGA 600 ##

Put your amiga 600 roms in this directory.

Rom files must be files of the following types:
- ADF Disk (*.adf, zipped or 7zipped)
- IPF Disk (*.ipf, zipped)
- Amiga Forever RP9 packages (*.rp9)
- WHDL Folders (folder, zip or lha)

UAE files (*.uae) may be used to specify configuration for a particular game.

This system requires BIOS to work (kickstart 1.3 & kickstart 2.0 for WHDL)

Documentation can be found here : 
https://github.com/recalbox/recalbox-os/wiki/Amiga-emulation-on-Recalbox-(EN)
