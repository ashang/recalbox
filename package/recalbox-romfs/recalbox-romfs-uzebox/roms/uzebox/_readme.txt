## RECALBOX - SYSTEM UZEBOX ##

Put your uzebox roms in this directory.

Rom files must have a ".uze/.zip" extension.

This system allows to use compressed roms in .zip.
But, it is only an archive. Files inside the .zip must match with extensions mentioned above.
Each .zip file must contain only one compressed rom.
